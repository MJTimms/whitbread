module.exports = function(grunt) {
    grunt.initConfig({
        sass: {
            dev: { 
              files: {
                'styles/style.css': 'styles/style.scss'
              }
            }
        },

        cssmin: {
            build: {
                src: 'styles/style.css',
                dest: 'styles/style.min.css'
            }
        },

        uglify: {
            build: {
                files: {
                    'scripts/javascript.min.js': ['scripts/javascript.js']
                }
            }
        },

        watch: {
            sass: {
                files: '**/*.scss',
                tasks: ['css']
            },

            uglify: {
                files: 'scripts/javascript.js',
                tasks: ['uglify']
            }
        } 
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['sass']);
    grunt.registerTask('css', ['sass', 'cssmin']);
    grunt.registerTask('js', ['uglify']);
};