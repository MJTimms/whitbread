*** Whitbread FED Test Summary ***

**Project Environment**
Created project with all files I knew I would need.
Set up Grunt as a task runner to compile SASS to a CSS file and minify CSS and JS file.
Had there been a need for multiple JS/CSS files, could also have set up concatination of files, however, for this task I did not feel there was a need to split CSS or JS up more.

**Foursquare API**
With no prior experience using Foursquares API, I skimmed through the API documentation. Identified the key API calls which gave me a better understanding of what the task would likely require.

**HTML Structure**
Created a quick sketch wireframe to give me an idea towards the over all goal for the layout, and created basic HTML structure.

**Javascript**
I opted to utilize the fetch Javascript ES6 AJAX request mothod to access the Foursquare API. This method was well documented in the Foursquare API.
The fetch method returns an unusable promise. I converted the promise to a JSON object to loop though, therefore allowing my to dynamically render the Venue results of the location search into the DOM 
without the need for page reloading, resulting in a fast, user friendly, responsive interface with minimal load times.
Added the functionality for users to be able to search a location based on recommendations or popularity.
Handled API request errors, giving the user some suggestions of how to fix the error.
