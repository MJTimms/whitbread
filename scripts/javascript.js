//Foursqaure clientID and client Secret
const clientId = "UFBGZHLDEHEOFU1XPDFHAHSDINOSXD5CGOR5PRAYXNV1ULDC";
const clientSecret = "MMPIJGWJWQ10MK3FWHXYXPD1MFVZRZWTUGYRWECUJQW1YF4A";

//Setting default value for search type (Recommended or Popular).
var searchType = "explore";

//Event listener for location submit button.
document.getElementById("submitLocation")
    .addEventListener("click", sendAPIRequest);
//Event listener for changing search type to recommended.
document.getElementById("recommended")
    .addEventListener("click", setRecommended);
//Event listener for changing search type to popular.
document.getElementById("popular")
    .addEventListener("click", setPopular);

function sendAPIRequest(){
    var location = document.getElementById("searchTerm").value;
    /* 
        Send API request.
        ***** CHANGE FOR PUBLISH ***** Limted to 4 results 
    */
    fetch("https://api.foursquare.com/v2/venues/"+searchType
    +"?client_id="+clientId
    +"&client_secret="+clientSecret
    +"&v=20190906"
    +"&near="+location)
    //Convert promise response to usable JSON object.
    .then(response => response.json())
    //Dynamically Render Results from JSON object.
    .then(data => {

        console.log(data);

        //Setting default JSON data path depending on search type.
        var jsonPath;
        if (searchType === "explore"){
            jsonPath = data["response"].groups["0"].items;
        }
        else{
            jsonPath = data["response"].venues;
        }

        //Hide search form.
        var searchForm = document.getElementById("searchForm");
        searchForm.classList.add("hidden");
       //Show search breadcrumbs
        var searchBreadcrumb = document.getElementById("searchBreadcrumb");
        searchBreadcrumb.classList.remove("hidden");
       //Append data to searchBreadcrumb

        
        var breadcrumbs = document.createElement("p");
        breadcrumbs.innerHTML = "Showing all available results for <strong>" + location
        +"</strong>. <span id='changeSearch'>[Change Location]</span>";
        searchBreadcrumb.appendChild(breadcrumbs);

        document.getElementById("changeSearch").addEventListener("click", setSearch);

       //Clear any existing search results.
       document.getElementById("resultsContainer").innerHTML = "";

        /*  
            Loop each result.
            Dynamically create elements for each results and render to page.
        */ 
        for (x in jsonPath){
            //Create result container.
            var result = document.createElement("div");
            result.setAttribute("class", "result");
            //Create results image container.
            var imgContainer = document.createElement("div");
            imgContainer.setAttribute("class", "result--img-container");
            //Create default image for result.
            var img = document.createElement("img");
            img.setAttribute("src", "./images/default-icon.png");
            img.setAttribute("width", "100");
            img.setAttribute("class", "result--img");
            //Append default image to image container.
            imgContainer.appendChild(img);
            //Create text container for result title.
            var titleContainer = document.createElement("div");
            titleContainer.setAttribute("class", "result--title-container");
            //Create result title.
            var title = document.createElement("p");
            title.setAttribute("class", "result--title");
            //Set result title value depending on search type.
            if (searchType === "explore"){
                title.innerHTML = jsonPath[x].venue["name"];
            }
            else{
                title.innerHTML = jsonPath[x].name;
            }
            //Append result title to title container.
            titleContainer.appendChild(title);
            //Append image container to result.
            result.appendChild(imgContainer);
            //Append text container to result.
            result.appendChild(titleContainer);

            //Append result to result container.
            document.getElementById("resultsContainer").appendChild(result);
        } 
    })  
    .catch(function(){
        // Code for handling errors

        //Clear any existing search results
        document.getElementById("resultsContainer").innerHTML = "";

        //Crate container for error messages
        var error = document.createElement("div");
        error.setAttribute("class", "result--error-container");
        //Create title for error message
        var errorMsg = document.createElement("h2");
        errorMsg.setAttribute("class", "result--error-title");
        errorMsg.innerHTML = "Ooops! Looks like we hit an issue...";
        //Create contianer for suggested fixes for the error
        var resultErrorContainer = document.createElement("div");
        resultErrorContainer.setAttribute("class", "result--error-suggestions");
        //Create title for suggested fixes
        var resultErrorText = document.createElement("p");
        resultErrorText.setAttribute("class", "result--error-text");
        resultErrorText.innerHTML = "Let's get you back on your way! Please check the suggestions below for common reasons for this error.";
        //Create list element to display suggested fixes for the error
        var resultErrorList = document.createElement("ul");
        resultErrorList.setAttribute("class", "result--errors-list");

        //Array of possible reasons for error
        var errorReasons = [
            "Invalid search term. Please check your search and try again.",
            "Unstable connection. Please check your internet connection.",
            "Other reason #1.",
            "Other reason #2.",
            "Other reason #3."
        ];

        var errorLength = errorReasons.length;

        //Loop through Array appending each reason as an <li> into the previously created <ul>
        for (var y in errorReasons){
            var errorReason = document.createElement("li");
            errorReason.setAttribute("class", "result--error-item")
            errorReason.innerHTML = errorReasons[y];
            resultErrorList.appendChild(errorReason);
        }

        resultErrorContainer.appendChild(resultErrorText);
        resultErrorContainer.appendChild(resultErrorList);

        error.appendChild(errorMsg);
        error.appendChild(resultErrorContainer);

        document.getElementById("resultsContainer").appendChild(error);
    });
}

function setSearch(){
    //Clear any existing search results.
    document.getElementById("resultsContainer").innerHTML = "";

    //Show search form.
    var searchForm = document.getElementById("searchForm");
    searchForm.classList.remove("hidden");
   //Hide search breadcrumbs
    var searchBreadcrumb = document.getElementById("searchBreadcrumb");
    searchBreadcrumb.classList.add("hidden");
    searchBreadcrumb.innerHTML = "";
   //Append data to searchBreadcrumb

   //Clear any existing search results.
   document.getElementById("resultsContainer").innerHTML = "";
}

//Set search type to "explore" for recommended.
function setRecommended(){
    searchType = "explore";
    var recommendBtn = document.getElementById("recommended");
    var popularBtn = document.getElementById("popular");
    recommendBtn.classList.add("form--btn_active");
    popularBtn.classList.remove("form--btn_active");
}

//set Search type to "Search" for popular.
function setPopular(e){
    searchType = "search";
    var recommendBtn = document.getElementById("recommended");
    var popularBtn = document.getElementById("popular");
    recommendBtn.classList.remove("form--btn_active");
    popularBtn.classList.add("form--btn_active");
}